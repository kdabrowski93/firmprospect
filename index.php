<!DOCTYPE html>
<html>
    <head>
        <title>Firm Prospects</title>
        <meta name="keywords" content="Firm Prospects, legal recruiter, attorney lists, attorney jobs, law firm jobs">
        <meta name="description" content="Database of attorney biographies, law firm jobs, law firm analytics designed for legal recruiters.">
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
        <link href="img/favicon.ico" type="image/x-icon" rel="icon" />
        <link rel="stylesheet" type="text/css" href="style.css?ver=2.2">
        <link rel="stylesheet" type="text/css" href="rwd.css?ver=2.2">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,100,700,900,300italic' rel='stylesheet' type='text/css'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
        <script src="script.js?ver=2.2"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <meta name="og:title" content="Firm Prospects">
        <meta name="og:type" content="website">
        <meta name="og:url" content="https://www.firmprospects.com/">
        <meta name="og:image" content="https://www.firmprospects.com/img/logo.png">
        <meta name="og:site_name" content="firmprospects.com">
        <meta name="og:description" content="Database of attorney biographies, law firm jobs, law firm analytics designed for legal recruiters.">
    </head>
    <body>
        <div id="hidden-header"></div>
        <div id="header">
            <div class="site-content">
                <div class="top">
                    <a href="">
                        <img class="logo" src="img/logo.png" />
                    </a>
                    <div class="header-top-right">
                        <span class="text">
                            1-844-FIRM-PRO
                        </span>
                        <span class="text">
                            (1-844-347-6776)
                        </span>
                        <div class="top-buttons">
                            <a href="contact">
                                <img src="tmp/vvv.png" />
                                <span>
                                    CONTACT
                                </span>
                            </a>
                            <a href="login">
                                <img src="tmp/qqq.png" />
                                <span>
                                    LOGIN
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="bottom">
                    <ul>
                        <li class="litarget">JOBS</li>
                        <li class="litarget">CANDIDATES</li>
                        <li class="litarget">ANALYTICS</li>
                        <li class="litarget">CRM</li>
                    </ul>
                    <div class="clear"></div>
                    <p>
                        A comprehensive database of law firm jobs, attorney profiles, and law firm analytics designed exclusively for legal recruiters. 
                        Easily access our data from the Firm Prospects platform or our custom designed CRM.
                    </p>
                    <div class="header-buttons">
                        <a href="">
                            <div class="header-buttons-img">
                                <img src="tmp/dddd.png" />
                            </div>
                            <span>FREE TRIAL</span>
                        </a>
                        <a href="">
                            <div class="header-buttons-img">
                                <img src="tmp/dddd.png" />
                            </div>
                            <span>WATCH DEMO</span>
                        </a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="content">
            <div class="row1">
                <div class="site-content">
                    <div class="flex" data-target="1">
                        <div class="fleximage">
                            <img class="row1normal" src="tmp/awsd.png" />
                            <img class="row1hover" src="tmp/werwe.png" />
                        </div>
                        <div class="flextext">
                            <span>Jobs</span>
                            National and international <br class="fullbr" />job listings with real-time <br class="fullbr" />email alerts.
                        </div>
                    </div>
                    <div class="flex" data-target="2">
                        <div class="fleximage">
                            <img class="row1normal" src="tmp/ssssss.png" />
                            <img class="row1hover" src="tmp/dsffsd.png" />
                        </div>
                        <div class="flextext">
                            <span>Candidates</span>
                            Accurately categorized attorney <br class="fullbr" />
profiles from nearly 2000 <br class="fullbr" />
hand-picked law firms.
                        </div>
                    </div>
                    <div class="flex" data-target="3">
                        <div class="fleximage">
                            <img class="row1normal" src="tmp/rrrrree.png" />
                            <img class="row1hover" src="tmp/fgfgr.png" />
                        </div>
                        <div class="flextext">
                            <span>Analytics</span>
                            Unparalleled law firm <br class="fullbr" />
analytics for every firm in our <br class="fullbr" />
database.
                        </div>
                    </div>
                    <div class="flex" data-target="4">
                        <div class="fleximage">
                            <img class="row1normal" src="tmp/trerr.png" />
                            <img class="row1hover" src="tmp/qweeeed.png" />
                        </div>
                        <div class="flextext">
                            <span>CRM Integration</span>
                            Fully-integrated with our <br class="fullbr" />
CRM/ATS designed exclusively <br class="fullbr" />
for legal recruiters.
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="row2 bluebox">
                <div class="site-content">
                    
                    <div class="bluebox-right">
                        <div class="bluebox-right-top">
                            <div class="whitecircleout">
                                <div class="whitemouse"></div>
                                <div class="whitecircle">
                                    <img src="tmp/cdvdg.png" />
                                </div>
                            </div>
                            <span>Jobs</span>
                            <div class="clear"></div>
                        </div>
                        <div class="bluebox-right-bottom">
                            Our jobs database is packed with job listings from 700 national and international law firms, and we provide real-time job updates via email alerts throughout the business day. As with our candidate profiles, an attorney or legal search professional has accurately categorized each job in our database.
                        </div>
                    </div>
                    <div class="bluebox-left">
                        <img src="img/row1left.png" />
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="row3 whitebox">
                <div class="site-content">
                    <div class="whitebox-left">
                        <div class="whitebox-left-top">
                            <div class="bluecircleout">
                                <div class="whitemouse"></div>
                                <div class="bluecircle">
                                    <img src="tmp/fdgdfg.png" />
                                </div>
                            </div>
                            <span>Candidates</span>
                        </div>
                        <div class="whitebox-left-bottom">
                            With accurately categorized, relevant candidates from nearly 2,000 hand-picked law firms, you can instantly target the most qualified group of attorneys for your job search. Every one of our candidates has a fully searchable bio and has been categorized by practice area and specialty by attorneys and legal search 
professionals.
                        </div>
                    </div>
                    <div class="whitebox-right">
                        <img src="img/row2left.png" />
                    </div>
                <div class="clear"></div>
                </div>
            </div>
            <div class="row4 bluebox">
                <div class="site-content">
                    
                    <div class="bluebox-right">
                        <div class="bluebox-right-top">
                            <div class="whitecircleout">
                                <div class="whitemouse"></div>
                                <div class="whitecircle">
                                    <img src="tmp/gfhgh.png" />
                                </div>
                            </div>
                            <span>Analytics</span>
                            <div class="clear"></div>
                        </div>
                        <div class="bluebox-right-bottom">
                            We provide detailed analytics for each law firm in our database, 
including information on practice areas, specialties, lateral movements in and out of firms and much more. 
                        </div>
                    </div>
                    <div class="bluebox-left">
                        <img src="img/row3left.png" />
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="row5 whitebox">
                <div class="site-content">
                    <div class="whitebox-left">
                        <div class="whitebox-left-top">
                            <div class="bluecircleout">
                                <div class="whitemouse"></div>
                                <div class="bluecircle">
                                    <img src="tmp/plkiol.png" />
                                </div>
                            </div>
                            <span>CRM Integration</span>
                        </div>
                        <div class="whitebox-left-bottom">
                            Our data is fully integrated with a custom designed CRM/ATS built 
exclusively for the legal recruiting industry on the world's #1 CRM platform--SalesForce.  With our CRM option, you will never have to manually upload candidates or deal with stale data.
                        </div>
                    </div>
                    <div class="whitebox-right">
                        <img src="img/row4left.png" />
                    </div>
                <div class="clear"></div>
                </div>
            </div>
        </div>
        <div id="footer">
            <div class="footer-top">
                <div class="site-content">
                    
                    <div class="footer2">
                        <div class="footer-title">FAQ</div>
                        <!--a class="accordion" data-accordion="0">
                            <span>What exactly is Firm Prospects?</span>
                            <img src="img/arrow.png" />
                            <div id="accordion0" class="accordions">
                                Firm Prospects is a comprehensive database of law firm jobs, candidate profiles and law firm analytics designed exclusively for legal recruiters.  Our database can be accessed directly from the Firm Prospects interface or from our custom designed CRM/ATS built on the SalesForce platform.
                            </div>
                        </a>
                        
                        <a class="accordion" data-accordion="1">
                            <span>What are Real-Time Job Alerts?</span>
                            <img src="img/arrow.png" />
                        </a>
                        <div id="accordion1" class="accordions">
                            We update our database continually throughout the business day and notify our users via email about any new job added to the database.  This gives our users an advantage over the competition and limits the number of times that you'll hear "I've already heard about this position" from candidates--which can mean more placements for your search firm!
                        </div>
                        <a class="accordion" data-accordion="2">
                            <span>What information is provided with candidate profiles?</span>
                            <img src="img/arrow.png" />
                        </a>
                        <div id="accordion2" class="accordions">
                            Our attorney database contains detailed information about every candidate. But we do more than just scrape the data: an attorney or search firm professional organizes every one of the bios in our database by practice area and specialty, breaks out any advanced degrees the attorney might hold, list any honors received, and more, making the search process much more efficient.  You can also add your own notes to profiles, create worklists, and instantly find matching jobs for any candidate.
                        </div>
                        <a class="accordion" data-accordion="3">
                            <span>What is the General Inquiry search feature?</span>
                            <img src="img/arrow.png" />
                        </a>
                        <div id="accordion3" class="accordions">
                             Our platform allows users to include "General Inquiries" in the search results.  These are firms that don't always (or ever) post jobs on their webste but do accept job applications from search firms.  This allows you to quickly and easily put together a list of firms for a candidate that includes both active job postings as well as other firms you might want to reach out to on a more speculative basis.
                        </div>
                        <a class="accordion" data-accordion="4">
                            <span>How many firms does Firm Prospects currently monitor?</span>
                            <img src="img/arrow.png" />
                        </a>
                        <div id="accordion4" class="accordions">
                            We monitor nearly 1,800 law firms and counting.  Of those, currently about 650 law firms list jobs on their website. We focus on the top firms, including specialized boutiques, Amlaw 200, NLJ 250 Vault Firms, and more.  Our goal is not to have every law firm in our database, but instead to focus on those firms that are relevant to the legal recruiting industry.  
                        </div>
                        <a class="accordion" data-accordion="5">
                            <span>How much does a subscription to Firm Prospects cost?</span>
                            <img src="img/arrow.png" />
                        </a>
                        <div id="accordion5" class="accordions">
                            The cost of an annual subscription varies depending upon the number of users you will have at your firm and the geographic locations you want to cover.  For more detailed pricing information, you can reach us at support@firmprospects.com or 781.290.0101 ext. 711.
                        </div>
                        <a class="accordion" data-accordion="6">
                            <span>What geographical area do you cover?</span>
                            <img src="img/arrow.png" />
                        </a>
                        <div id="accordion6" class="accordions">
                            We currently list both national and international jobs.  On the candidate side, we have included all U.S. and London-based candidates.
                        </div>
                        <a class="accordion" data-accordion="7">
                            <span>Can you subscribe to a single database (jobs or candidates)?</span>
                            <img src="img/arrow.png" />
                        </a>
                        <div id="accordion7" class="accordions">
                            Yes, if you are interested in only jobs or only candidate bios, we can set you up with a subscription that matches your needs.
                        </div-->
                        <div id="accordion">
  <h3>What exactly is Firm Prospects?</h3>
  <div>
    <p>
    Firm Prospects is a comprehensive database of law firm jobs, candidate profiles and law firm analytics designed exclusively for legal recruiters.  Our database can be accessed directly from the Firm Prospects interface or from our custom designed CRM/ATS built on the SalesForce platform.
    </p>
  </div>
  <h3>What are Real-Time Job Alerts?</h3>
  <div>
    <p>
    We update our database continually throughout the business day and notify our users via email about any new job added to the database.  This gives our users an advantage over the competition and limits the number of times that you'll hear "I've already heard about this position" from candidates--which can mean more placements for your search firm!
    </p>
  </div>
  <h3>What information is provided with candidate profiles?</h3>
  <div>
    <p>
    Our attorney database contains detailed information about every candidate. But we do more than just scrape the data: an attorney or search firm professional organizes every one of the bios in our database by practice area and specialty, breaks out any advanced degrees the attorney might hold, list any honors received, and more, making the search process much more efficient.  You can also add your own notes to profiles, create worklists, and instantly find matching jobs for any candidate.
    </p>
  </div>
  <h3>What is the General Inquiry search feature?</h3>
  <div>
    <p>
    Our platform allows users to include "General Inquiries" in the search results.  These are firms that don't always (or ever) post jobs on their webste but do accept job applications from search firms.  This allows you to quickly and easily put together a list of firms for a candidate that includes both active job postings as well as other firms you might want to reach out to on a more speculative basis.
    </p>
  </div>
  <h3>How many firms does Firm Prospects currently monitor?</h3>
  <div>
    <p>
    We monitor nearly 1,800 law firms and counting.  Of those, currently about 650 law firms list jobs on their website. We focus on the top firms, including specialized boutiques, Amlaw 200, NLJ 250 Vault Firms, and more.  Our goal is not to have every law firm in our database, but instead to focus on those firms that are relevant to the legal recruiting industry.  
    </p>
  </div>
  <h3>How much does a subscription to Firm Prospects cost?</h3>
  <div>
    <p>
    The cost of an annual subscription varies depending upon the number of users you will have at your firm and the geographic locations you want to cover.  For more detailed pricing information, you can reach us at support@firmprospects.com or 781.290.0101 ext. 711.
    </p>
  </div>
  <h3>What geographical area do you cover?</h3>
  <div>
    <p>
    We currently list both national and international jobs.  On the candidate side, we have included all U.S. and London-based candidates.
    </p>
  </div>
  <h3>Can you subscribe to a single database (jobs or candidates)?</h3>
  <div>
    <p>
    Yes, if you are interested in only jobs or only candidate bios, we can set you up with a subscription that matches your needs.
    </p>
  </div>
</div>
                    </div>
                    <div class="footer1">
                        <div class="footer-title">CLIENT TESTIMONIALS</div>
                        <?php $rand = rand(1, 1); ?>
                        <?php if($rand == 1): ?>
                        <span class="opinion">
                            "As the Director of Legal Recruiting at a legal search firm, I can say that this is truly a great resource for any legal recruiter. The functionality is better than its competitors and it makes finding the right jobs for candidates a more efficient process."
                        </span>
                        <span class="opinionauthor">
                             Terri LeComte - Boston, MA 
                        </span>
                        <?php elseif($rand == 2): ?>
                        <span class="opinion">
                             "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
                        </span>
                        <span class="opinionauthor">
                             Test Test 
                        </span>
                        <?php endif; ?>
                    </div>
                    <div class="footer3">
                        <div class="footer-title">CONTACT</div>
                        <span class="span1">
                            Firm Prospects, LLC
                        </span>
                        <span class="span2">
                            1-844-347-6776
                        </span>
                        <span class="span3">
                            support@firmprospects.com 
                        </span>
                        <div class="socials">
                            <span>FIND US:</span>
                            <a href="">
                                <img class="socialrwd" src="tmp/ewre.png" />
                                <img class="socialnormal" src="img/fb.png" />
                            </a>
                            <a href="">
                                <img class="socialrwd" src="tmp/dsf.png" />
                                <img class="socialnormal" src="img/lin.png" />
                            </a>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="footer-bottom">
                COPYRIGHT ©2016 FIRM PROSPECTS. ALL RIGHTS RESERVED
            </div>
        </div>
        <div id="topper">
            <img src="img/toparrow.png" />
        </div>
    </body>
</html>

